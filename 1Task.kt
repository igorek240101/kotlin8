// Сделать программу, которая запускает в фоновом потоке метод печати «World» с задержкой в 1 сек. В основном потоке запускаем печать «Hello,» с задержкой в 2 сек., блокируем поток
fun main() {
    val backgroundThread = Thread {
        Thread.sleep(1000L)
        println("World")
    }

    backgroundThread.start()
    Thread.sleep(2000L)
    println("Hello,")
    backgroundThread.join()
}
